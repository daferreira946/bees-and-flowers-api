<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bees')->insert([
            [
                'name' => 'Uruçu',
                'specie' => 'Melipona scutellaris',
            ],
            [
                'name' => 'Uruçu-Amarela',
                'specie' => 'Melipona rufiventris',
            ],
            [
                'name' => 'Guarupu',
                'specie' => 'Melipona bicolor',
            ],
            [
                'name' => 'Iraí',
                'specie' => 'Nannotrigona testaceicornes',
            ],
        ]);
    }
}
