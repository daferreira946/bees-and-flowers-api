# API

## Rodando o projeto

## Requerimentos

* PHP 7.4^
* Composer

## Comandos

* Rodar na pasta raiz do projeto
    * ` composer install `
    * ` php artisan migrate:refresh `
    * ` php artisan db:seed `
    * ` php -S localhost:8000 -t public`
    
* Para testar
    * Conferir se no arquivo composer.json tem o script, se sim digite:
        * composer test
    * Se não, digite:
        * .\vendor\bin\phpunit --testdox --verbose (Cli melhorado)

## Postman Collection
* https://www.getpostman.com/collections/2736401b2dfa2d45e9f3
* Necessita do Postman Agent

## Rotas

### Bees
* GET /bees 
    * Retorna todas as abelhas
* GET /bees/{id} 
    *Retorna abelha referente ao id passado

### Flowers
* GET /flowers
    * Retorna todas as flores paginadas com 12 itens e os controles de navegação, permite o uso de filtro por abelha (adicionando bee={id} na query string) e por meses de floração (adicionando months={numeroDoMes};{outroMes};{outroMes}).
    * Exemplo: GET /flowers?bee=1&months=1;2;3
* GET /flowers/{id} 
    * Retorna abelha referente e seus relacionamentos
* POST /flowers 
    * Cadastra uma nova flor.
    * Campos:
        * name [Nome] => required, string
        * specie [Espécie] => required, string, unique
        * description [Descrição] => nullable, string
        * image [Imagem] => required, image
        * bees [Abelhas] => required, string, regex:/^(\d+,)*\d+$/
        * months [Meses] => required, string, regex:/^(\d+,)*\d+$/

-------------------------------------------------------------

## Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

### Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

### Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

### Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
